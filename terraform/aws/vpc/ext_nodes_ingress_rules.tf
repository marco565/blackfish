resource "aws_security_group_rule" "nodes_from_nodes_tcp_ext" {
    type = "ingress"
    from_port = 32768
    to_port = 60999
    protocol = "tcp"
    security_group_id = "${aws_security_group.ext_nodes.id}"
    cidr_blocks = ["${compact(split(",",var.external_nodes_subnets))}"]
}

resource "aws_security_group_rule" "nodes_from_nodes_udp_ext" {
    type = "ingress"
    from_port = 32768
    to_port = 60999
    protocol = "udp"
    security_group_id = "${aws_security_group.ext_nodes.id}"
    cidr_blocks = ["${compact(split(",",var.external_nodes_subnets))}"]
}

resource "aws_security_group_rule" "nodes_from_node_consul_cluster_tcp_ext" {
    type = "ingress"
    from_port = 8300
    to_port = 8302
    protocol = "tcp"
    security_group_id = "${aws_security_group.ext_nodes.id}"
    cidr_blocks = ["${compact(split(",",var.external_nodes_subnets))}"]
}

resource "aws_security_group_rule" "nodes_from_node_consul_cluster_udp_ext" {
  type = "ingress"
  from_port = 8300
  to_port = 8302
  protocol = "udp"
  security_group_id = "${aws_security_group.ext_nodes.id}"
  cidr_blocks = ["${compact(split(",",var.external_nodes_subnets))}"]
}

resource "aws_security_group_rule" "nodes_from_node_consul_agent_ext" {
  type = "ingress"
  from_port = 8500
  to_port = 8501
  protocol = "tcp"
  security_group_id = "${aws_security_group.ext_nodes.id}"
  cidr_blocks = ["${compact(split(",",var.external_nodes_subnets))}"]
}

resource "aws_security_group_rule" "nodes_from_node_consul_dns_tcp_ext" {
    type = "ingress"
    from_port = 8600
    to_port = 8600
    protocol = "tcp"
    security_group_id = "${aws_security_group.ext_nodes.id}"
    cidr_blocks = ["${compact(split(",",var.external_nodes_subnets))}"]
}

resource "aws_security_group_rule" "nodes_from_node_consul_dns_udp_ext" {
    type = "ingress"
    from_port = 8600
    to_port = 8600
    protocol = "udp"
    security_group_id = "${aws_security_group.ext_nodes.id}"
    cidr_blocks = ["${compact(split(",",var.external_nodes_subnets))}"]
}

resource "aws_security_group_rule" "nodes_from_node_dns_tcp_ext" {
    type = "ingress"
    from_port = 53
    to_port = 53
    protocol = "tcp"
    security_group_id = "${aws_security_group.ext_nodes.id}"
    cidr_blocks = ["${compact(split(",",var.external_nodes_subnets))}"]
}


resource "aws_security_group_rule" "nodes_from_node_dns_udp_ext" {
    type = "ingress"
    from_port = 53
    to_port = 53
    protocol = "udp"
    security_group_id = "${aws_security_group.ext_nodes.id}"
    cidr_blocks = ["${compact(split(",",var.external_nodes_subnets))}"]
}

resource "aws_security_group_rule" "nodes_from_node_docker_ext" {
    type = "ingress"
    from_port = 2375
    to_port = 2376
    protocol = "tcp"
    security_group_id = "${aws_security_group.ext_nodes.id}"
    cidr_blocks = ["${compact(split(",",var.external_nodes_subnets))}"]
}

resource "aws_security_group_rule" "nodes_from_node_swarm_ext" {
    type = "ingress"
    from_port = 4000
    to_port = 4000
    protocol = "tcp"
    security_group_id = "${aws_security_group.ext_nodes.id}"
    cidr_blocks = ["${compact(split(",",var.external_nodes_subnets))}"]
}

resource "aws_security_group_rule" "nodes_from_node_docker_network_tcp_ext" {
    type = "ingress"
    from_port = 7946
    to_port = 7946
    protocol = "tcp"
    security_group_id = "${aws_security_group.ext_nodes.id}"
    cidr_blocks = ["${compact(split(",",var.external_nodes_subnets))}"]
}

resource "aws_security_group_rule" "nodes_from_node_docker_network_udp_ext" {
  type = "ingress"
  from_port = 7946
  to_port = 7946
  protocol = "udp"
  security_group_id = "${aws_security_group.ext_nodes.id}"
  cidr_blocks = ["${compact(split(",",var.external_nodes_subnets))}"]
}

resource "aws_security_group_rule" "nodes_from_node_docker_network2_udp_ext" {
  type = "ingress"
  from_port = 4789
  to_port = 4789
  protocol = "udp"
  security_group_id = "${aws_security_group.ext_nodes.id}"
  cidr_blocks = ["${compact(split(",",var.external_nodes_subnets))}"]
}

resource "aws_security_group_rule" "nodes_from_node_docker_registry_ext" {
  type = "ingress"
  from_port = 5000
  to_port = 5000
  protocol = "tcp"
  security_group_id = "${aws_security_group.ext_nodes.id}"
  cidr_blocks = ["${compact(split(",",var.external_nodes_subnets))}"]
}

resource "aws_security_group_rule" "nodes_from_node_web_ext" {
  type = "ingress"
  from_port = 80
  to_port = 80
  protocol = "tcp"
  security_group_id = "${aws_security_group.ext_nodes.id}"
  cidr_blocks = ["${compact(split(",",var.external_nodes_subnets))}"]
}

resource "aws_security_group_rule" "nodes_from_node_web_ssl_ext" {
  type = "ingress"
  from_port = 443
  to_port = 443
  protocol = "tcp"
  security_group_id = "${aws_security_group.ext_nodes.id}"
  cidr_blocks = ["${compact(split(",",var.external_nodes_subnets))}"]
}

resource "aws_security_group_rule" "nodes_from_node_control_ssl_ext" {
  type = "ingress"
  from_port = 4443
  to_port = 4443
  protocol = "tcp"
  security_group_id = "${aws_security_group.ext_nodes.id}"
  cidr_blocks = ["${compact(split(",",var.external_nodes_subnets))}"]
}
