output "bastion_pub_ip" {
  value = "${openstack_compute_instance_v2.bastion.network.0.fixed_ip_v4}"
}

output "bastion_priv_ip" {
  value = "${openstack_compute_instance_v2.bastion.network.1.fixed_ip_v4}"
}

output "blackfish_image_id" {
  value = "${openstack_image_v2.blackfish.id}"
}

output "bastion_image_id" {
  value = "${openstack_image_v2.bastion.id}"
}

output "keypair" {
  value = "${openstack_compute_keypair_v2.bf_keypair.name}"
}

output "registry_container_name" {
  value = "${openstack_objectstorage_container_v1.bf_registry.name}"
}

output "nodes_security_group" {
  value = "${openstack_networking_secgroup_v2.nodes_sg.id}"
}


