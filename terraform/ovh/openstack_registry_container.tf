resource "openstack_objectstorage_container_v1" "bf_registry" {
  region = "${var.ovh_region}"
  name = "${var.stack_name}_bf_registry"

  metadata {
    stack_name = "${var.stack_name}"
    blackfish_type = "registry_container"
  }

  container_read  = "${var.os_username}"
  container_write = "${var.os_username}"

}
